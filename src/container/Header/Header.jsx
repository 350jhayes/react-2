import React from 'react';
import { SubHeading } from '../../components';
import { images } from '../../constants';

import './Header.css';

const Header = () => (
  <div className="app__header app__wrapper section__padding" id="home">
    <div className="app__wrapper_info">
      <SubHeading title="Chase the new flavour" />
      <h1 className="app__header-h1">Core ingredients for a sophisticated culinary experience</h1>
      <p className="p__opensans" style={{ margin: '2rem 0' }}>Experience the vibrant essence of life through captivating tales and the joys of shared moments. Embrace the richness of diverse flavors that elevate the senses and ignite a delightful journey. Discover the harmonious blend of tradition and innovation, as we create unforgettable memories, all while embracing the bonds of friendship.</p>
      <button type="button" className="custom__button">Explore Menu</button>
    </div>

    <div className="app__wrapper_img">
      <img src={images.welcome} alt="header_img" />
    </div>

  </div>
);

export default Header;
