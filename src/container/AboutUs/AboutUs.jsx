import React from 'react';
import { images } from '../../constants';
import './AboutUs.css';

const AboutUs = () => (
  <div className="app__aboutus app__bg flex__center section__padding" id="about">
    <div className="app__aboutus-overlay flex__center">
      <img src={images.G} alt="g letter" />
    </div>

    <div className="app__aboutus-content flex__center">
      <div className="app__aboutus-content_aboutus">
        <h1 className="headtext__cormorant">About Us</h1>
        <img src={images.spoon} alt="about_spoon" className="spoon__img" />
        <p className="p__opensans">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed dignissim tristique est, auctor gravida elit suscipit vel. Ut vestibulum urna ac nibh euismod rutrum. Morbi in semper metus. Cras consequat elit sit amet nisi lacinia, non efficitur magna eleifend. Integer condimentum scelerisque sem, et eleifend ipsum faucibus ac. Fusce commodo, lectus sed dignissim cursus, dolor quam aliquam ex, in ullamcorper ligula urna vel nibh. Sed congue consequat neque, nec scelerisque felis condimentum id. Curabitur pulvinar orci quis felis vestibulum hendrerit.</p>
        <button tyoe="button" className="custom__button">Know More</button>
      </div>

      <div className="app__aboutus-content_knife flex__center">
        <img src={images.knife} alt="about_knife" />
      </div>

      <div className="app__aboutus-content_history">
        <h1 className="headtext__cormorant">Background</h1>
        <img src={images.spoon} alt="about_spoon" className="spoon__img" />
        <p className="p__opensans">Ultrices fringilla odio tellus justo volutpat sed. Arcu volutpat lobortis risus placerat habitasse mi interdum aliquam. Sed odio nec aliquet, adipiscing tempus ullamcorper lobortis morbi.</p>
        <button type="button" className="custom__button">Know More</button>
      </div>
    </div>
  </div>
);

export default AboutUs;
