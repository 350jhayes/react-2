import React from 'react';
import { SubHeading, MenuItem } from '../../components';
import { images, data } from '../../constants';
import './SpecialMenu.css';

const SpecialMenu = () => (
  <div className="app__sm flex__center">
    <div className="app__sm-title">
      <SubHeading title="Menu that fits you Appetite" />
      <h1 className="headtext__cormorant">Today Special</h1>
    </div>

    <div className="app__sm-menu">
      <div className="app__sm-menu_wine flex__center">
        <p className="app__sm-menu_heading">Wine & Beer</p>
        <div className="app__sm_menu_items">
          {data.wines.map((wine, index) => (
            <MenuItem key={wine.title + index} title={wine.title} price={wine.price} tags={wine.tags} />
          ))}
        </div>
      </div>

      <div className="app__sm-menu_img">
        <img src={images.menu} alt="menu__img" />
      </div>

      <div className="app__sm-menu_cocktails flex__center">
        <p className="app__sm-menu_heading">Cocktails</p>
        <div className="app__sm_menu_items">
          {data.cocktails.map((cocktail, index) => (
            <MenuItem key={cocktail.title + index} title={cocktail.title} price={cocktail.price} tags={cocktail.tags} />
          ))}
        </div>
      </div>
    </div>

    <div style={{ marginTop: 15 }}>
      <button type="button" className="custom_button">View More</button>
    </div>
  </div>
);

export default SpecialMenu;
